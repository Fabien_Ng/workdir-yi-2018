
# TP Realworld

## 6.3

### 6.3.1

commit 7a2e472d9d9e77ad7de8f4ace9e09d7597ee60f7

### 6.3.2

https://github.com/mkenney/docker-npm/blob/master/node-8-debian/Dockerfile

VOLUME :  /src

docker image pull mkenney/npm:node-8-debian

9af19a7f4e2c

###6.3.4

docker container run -ti --rm -v $(pwd):/src 9af19a7f4e2c npm install

docker container run -ti --rm -p 8000:8080 -v $(pwd):/src 9af19a7f4e2c npm run dev

## 6.4

### 6.4.1

89f9d974a6b5282b78517487b6cbf81ab1a538fc

### 6.4.2

docker image pull gradle:4.7.0-jdk8-alpine

f438b7d58d0a

VOLUME : /home/gradle/.gradle

### 6.4.3

docker volume create gradle-home

docker volume ls

DRIVER              VOLUME NAME
local               06f609229487406fc300fc1f53f2bd6fa3ca845c463e5162b4cecfd7e030843d
local               0cf8d93edd0e6c512e5cd678d32399335f9cc62cf085213dbafa4205bbd2db46
local               0dbd7bbc4fa6297a5bae8a0e6465fb3afe0610c056a7ff1cd0914faac836c835
local               162182ca96750f0e33b38b7c29812b54b7c9099b710b3c424fc885dc246f4776
local               1b9d5d13507800445e8c9134c578fd5367762736a021c872a7dc594712ed2f7e
local               1c93b2a5a469247dd0f6fb6a6059b9324fe7860f0bb63530088b971c1bfe7e3f
local               2beedd122cd27a2e982c24d6c3169d1ee363dbe22511e9776ac2bf0585042112
local               3888b04d1f91c9579a9eb2539bd7293599242d25dd99622845e7cb18ef221f73
local               4466fdcdb4fddc17d0fa11d570a0bf6c7ebe62d9b273c72a42be89ef411a4f78
local               4fc4439116cff24c4131aa5060bc1f2d6ac1824461ca452ffb284306f6ed8e16
local               75efabb5ee5f010e5b836a36c6922f232b9d4dfa40e3321ee57397142ae2cd2d
local               7ffd69f3632f430c800aefcb7c6d5696bad6916747a57086c67765c15f185041
local               adf89e2931cac354af7d208e1c0a8f14677431dacd4c6266d670a331219a3965
local               b22b16c409fa8c63d11580ae2abf2652654533687448a55ba92095a6c319472e
local               c037cabaf7d32a79a80f91077a31105436ea159329c42b146f6aada3e32e5c01
local               e469e2e5c1b426141e540d904c7f4a69a30ddfbade5c1610871a6deffd6d304c
local               ed8481b4b794c62810067bf6109c3f4db8f63a69b71bb15965dc7d50d3514d34
local               f28ca86133cca03f8d3af5af82327932fbd22021fb30d931d3c4ea3b4abd5433
local               fad7d2f9f6f1017433c38700d7ec8065f21777d86827634ec008273f0130685f
local               gradle-home
local               nginw-logs
local               nginx-logs


### 6.4.4

docker container run -ti --rm -w="/src" -v gradle-home:/home/gradle/.gradle -v $(pwd):/src f438b7d58d0a gradle build

### 6.4.5

docker container run -ti --rm -w="/src" -p 8001:8080 -v gradle-home:/home/gradle/.gradle -v $(pwd):/src f438b7d58d0a gradle bootRun

articles	[]
articlesCount	0

## 6.5

### 6.5.1 

articles	
0	
id	"2a227765-0407-44f0-95e4-f1967770afd8"
slug	"root"
title	"root"
description	"pas root"
body	"sudo"
favorited	false
favoritesCount	0
createdAt	"2018-05-16T13:35:41.332Z"
updatedAt	"2018-05-16T13:35:41.332Z"
tagList	[]
author	
username	"fabienng"
bio	""
image	"https://static.productionready.io/images/smiley-cyrus.jpg"
following	false
articlesCount	1

## 6.6

### 6.6.1

docker container run -ti --rm -v $(pwd):/src mkenney/npm:node-8-debian npm run build

favicons-c2a605fbc0e687b2e1b4b90a7c445cdd  index.html  static


### 6.6.2

docker container run -ti --rm -p 8000:80 -v $(pwd)/dist:/usr/share/nginx/html nginx:alpine 

### 6.6.3


